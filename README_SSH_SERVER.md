# SSH deamon/user configuration

The user for ssh tunnels establishment and the tunnels themselves have to be limited carefully in order to avoid server abuse.

The user has to be dedicated only for assistance, **do not repurpose an existing user!!!**

### User creation

Under Debian - like distros an easy way is to create a system user ( `assistenza` in this example):

`# adduser --system assistenza`

The newly created user `assistenza` will be member of the `nogroup` group and have `/usr/sbin/nologin` as shell, so no shell is executed upon login. 

### SSH configuration

You can choose two different configuration depending on server environment:

#### 1. Dedicated server

**You have a server dedicated to this assistance tool, with no other services running on it.** 

In this case, you can permit random listening port allocation, and consequently permit liberal port forwarding, but note that **DOS attacks are still possible, because there is no limit on the number of the listening ports that can be allocated!**  
This is the reason why **[2. Multipurpose server](#2-multipurpose-server)** configuration is preferred (and proposed by default).

You add limitations for user `assistenza` in ssh deamon configuration `/etc/sshd_config`:

```
Match User assistenza 
	X11Forwarding no
	PermitTTY no
	AllowAgentForwarding no
	PermitOpen localhost:*
```

The `PermitOpen localhost:*` in particular is **really important for avoiding abuse of ssh forwarding** for establishing arbitrary outbound connections.

In `assistenza.conf` the value of `REMOTE_LISTEN_PORT_RANGE` will be `random` .

#### 2. Multipurpose server

**Your server has valuable running services accessible on loopback interface!**

**... so you prefer to restrict port forwarding to the range dedicated to assistance.** 

This can be done prepending to the public key value in the `/home/assistenza/.ssh/authorized_key` file the opportune options, as in this example:

```
restrict,port-forwarding,permitlisten="localhost:12340",permitopen="localhost:12340",permitlisten="localhost:12341",permitopen="localhost:12341",permitlisten="localhost:12342",permitopen="localhost:12342",permitlisten="localhost:12343",permitopen="localhost:12343",permitlisten="localhost:12344",permitopen="localhost:12344" ssh-rsa AAAAB3....
```

* `restrict` as says the man page of `authorized_keys`: 

  > Enable all restrictions, i.e. disable port, agent and X11 forwarding, as well as disabling PTY allocation  and execution of \~/.ssh/rc.  If any future restriction capabilities are added to authorized_keys files  they will be included in this set.
* port-forwarding

  > Enable port forwarding previously disabled by the restrict option.
* `permitlisten="localhost:n",permitopen="localhost:n"` couples enable port listening allocation on,  and port fowarding to, respectively, port `n` on `localhost` only.

### SSH key generation

Given that the key will be disclosed to random users, setting a passphrase does not add real security, and the script is not designed to prompt for it, so you have to

`ssh-keygen -f id_rsa-assistenza`

leaving passphrase blank.

**Note**: A good practice would be **rotate the key** from time to time, **best of all after each usage**, if possible.

You enable the key copying the content of `id_rsa-assistenza.pub` in `/home/assistenza/.ssh/authorized_keys` on the server. See the [previous section on SSH configuration](#2-multipurpose-server) for how to set **important restrictions** for key usage.

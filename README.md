# assistenzalt

A simple tool for receiving remote assistance on a linux desktop.

Access to a configured remote ssh server is needed! See [README_SSH_SERVER.md](README_SSH_SERVER.md)

**New**: A [video howto](https://video.linuxtrent.it/w/xoc9ZRhYaLXZyTukyLa6Sm) (italian language) showing the setup procedure is available.

## Configuration

You **have** to modify (or receive form the assistant) the `assistenza.conf` file providing values for the following variables:

* `SSH_HOST` -> the hostname or ip of an host with a running ssh deamon
* `SSH_PORT` -> the port where the ssh deamon is listening
* `SSH_USER` -> the username authenticating via `SSH_KEY_FILE`
* `SSH_KEY_FILE` -> the filename of ssh private key for authentication
* `REMOTE_LISTEN_PORT_RANGE` -> the admissible listening port range on the server, or `random` if random port allocation is permitted.

And receive from the assistant the `SSH_KEY_FILE` file.

### For the assistant party

See [README_SSH_SERVER.md](README_SSH_SERVER.md) for importants hints on ssh server and user setup.

### Requirements on client side

**IMPORTANT**: **Ubuntu jammy (22.04 LTS)** (and possibly other distros as well) uses **Wayland** by default, and x11vnc is still not compatible. For getting assistance, switch to a traditional **Xorg** session at login (see how on the first image [here](https://linuxconfig.org/how-to-enable-disable-wayland-on-ubuntu-22-04-desktop))

**x11vnc** - on debian/ubuntu you can install the package with:

```
sudo apt-get install x11vnc
```

### Installation:

Run the installation script:

```
bash installa.sh
```

The script does the following tasks:

1. Copies (with appropriate permissions) **assistenza.sh**, **assistenza.conf**, **operator-512.png** and `SSH_KEY_FILE` in **/opt/lt/scripts/**
2. Copies **assistenza.desktop** in /usr/share/applications/**assistenzalt.desktop**

### Running the program:

Find "assistenza" on dektop GUI (the icon of an operator with headset and mic). If everything works as expected, a window will appear displaying the numeric code to be provided to the assistant.

## Giving assistance

The assistant party has to provide to the assisted:

* the materials in this repository (scripts, icon, desktop file)
* an `assistenza.conf` file correctly customized for the remote ssh server `my_ssh_server` enviroment
* the `SSH_KEY_FILE` mentioned in `assistenza.conf` ( `id_rsa-assistenza` in the following example)

And instruct the assisted in one time installation (installation of `x11vnc` and running `installa.sh`)

The assisted run the assistance tool, and tell the assistant the 'code', that is the **allocated remote listening port** ( `40251` in the following example). 

The assistant **can now establish the tunnel on his side** as `my_ssh_user` on `my_ssh_server` redirecting a local port ( `5900`  **in the example**) of his choice with:

```
ssh -L 5900:localhost:40251 -i id_rsa-assistenza -N my_ssh_user@my_ssh_server
```

**and running afterwards its vnc viewer** towards `localhost:5900`

#!/bin/bash
# Copyright (C) 2020-2022 Roberto Resoli <roberto@resolutions.it> e 
# Mitja Tavčar <mitja@mttv.it>
#
#This program is free software: you can redistribute it and/or modify it under 
#the terms of the GNU General Public License as published by the 
#Free Software Foundation, either version 3 of the License, or (at your option) 
#any later version.
#
#This program is distributed in the hope that it will be useful, but 
#WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
#or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
#more details.
#
#You should have received a copy of the GNU General Public License along with 
#this program. If not, see <https://www.gnu.org/licenses/>. 

. $(dirname $0)/assistenza.conf

SSH_KEY="$(dirname $0)/$SSH_KEY_FILE"

RUNNING_COUNT=$(pgrep -cf "ssh -i ${SSH_KEY}")
  if [ $RUNNING_COUNT -gt 0 ]; then
 	zenity --info --width=300 --height=100 --text "<big>Assistenza remota già avviata!</big>"
	echo "Running count $RUNNING_COUNT > 0; exit!"
	exit 1;
  fi 
  echo "Inizio Procedura"
# faccio partire x11vnc e cerco la porta
TMPF=`mktemp`
(
  echo 15
  echo -n "# Avvio x11vnc ..."
  x11vnc -forever -nevershared > /dev/null 2>&1 &

  echo 30
  echo "# Attendo numero porta."

  wait_max=10
  count=0
  while [ $count -lt $wait_max ] && [ "$VNC_PORT" == "" ]; do
	sleep 1
	VNC_PORT=$(ss -nt4lp | grep x11vnc | awk '{print $4}' | cut -d':' -f2);
	count=$(( count+1 ))
	echo $(( count + 30 ))
	echo "# Attendo numero porta."
  done

  if [ "$VNC_PORT" != "" ]; then
	echo 60
	echo "# x11vnc partito su porta $VNC_PORT"
	sleep 2
  else
	echo "# problemi con x11vnc"
	sleep 3
	exit 1
  fi

  echo 70
  echo "# Creo tunnel ssh ..."
 
  echo "Nessun codice disponibile!" > ${TMPF}
  if [ $REMOTE_LISTEN_PORT_RANGE != "random" ]; then
    #Cycle on admitted port range	
    for REMOTE_LISTEN_PORT in $(eval echo {$REMOTE_LISTEN_PORT_RANGE}); do
      ssh -i ${SSH_KEY} -l ${SSH_USER} -o "StrictHostKeyChecking=no" -o "ExitOnForwardFailure=yes" \
        -p ${SSH_PORT} \
        -R localhost:${REMOTE_LISTEN_PORT}:localhost:$VNC_PORT \
        -NTf ${SSH_HOST} && echo ${REMOTE_LISTEN_PORT} > ${TMPF} && break;
    done
  else
    #Run ssh in master command mode (-M) on unix socket (-S)
    ssh -i ${SSH_KEY} -l ${SSH_USER} -o "StrictHostKeyChecking=no" \
        -p ${SSH_PORT} -fNMS /tmp/ssh-assistenza ${SSH_HOST}
    sleep 1
    #Allocate a random listening port
    ssh -S /tmp/ssh-assistenza -O forward -R localhost:0:localhost:${VNC_PORT} assistenza > ${TMPF} 
  fi
  echo 100
  echo "# Fatto"
) | zenity --title "Inizializzazione Assistenza Remota" --progress --no-cancel --auto-close --width=300 --height=50

  #RPORT=$(grep Allocated ${TMPF} | awk '{print $3}')
  RPORT=$(cat $TMPF)
  rm $TMPF

 zenity --info --title "Assistenza remota avviata" --ok-label "Chiudi" --width=300 --height=150 --text "\nCodice da comunicare all\'assistenza:\n\n\t\t<tt><big><b>${RPORT}</b></big></tt>\n\n<b>Chiudendo questa finestra si termina la connessione con l'assistenza!</b>"


pkill x11vnc && echo "x11vnc stopped" || echo "Error stopping x11vnc"

if [ $REMOTE_LISTEN_PORT_RANGE != "random" ]; then
  pkill -f "ssh -i ${SSH_KEY}" && echo "ssh tunnel stopped" || echo "Error stopping ssh tunnel"
else
  ssh -S /tmp/ssh-assistenza -O exit assistenza && echo "ssh tunnel stopped" || echo "Error stopping ssh tunnel"
fi

